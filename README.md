Edit in [Cloud 9](https://ide.c9.io)

Download icons from: https://mapicons.mapsmarker.com/ and copy to the icons folder

Google Place search - supported types:
https://developers.google.com/places/supported_types#table1


The `<h1>` will not show up when you print the page because of the CSS rule:

    @media print {
      h1 {
        display: none;
      }
    }
    
The `Address` label will not show when you print the page because it has `class="gmnoprint"`